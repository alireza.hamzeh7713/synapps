import { useState } from "react";
import { GlobalStyle } from "./globalStyles";
import BackDrop from "./modules/shared/sideMenu/backDrop";
import Header from "./modules/shared/header";
import SideNav from "./modules/shared/sideMenu";
import HomePage from "./pages/homePage";
import  Routes from "./routes/index";

function App() {
  const [toggleSide, setToggleSide] = useState(false);

  const hanldeClickLogo = () => void setToggleSide(!toggleSide);

  const handleBackDrop = () => void setToggleSide(false);
  return (
    <>
      <Header LogoClick={hanldeClickLogo} />

      <SideNav showSide={toggleSide} />
      {toggleSide && <BackDrop click={handleBackDrop} />}
        <main>
       <Routes />
       </main>
      <GlobalStyle />
    </>
  );
}

export default App;
