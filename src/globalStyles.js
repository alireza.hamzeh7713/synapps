import styled, { createGlobalStyle } from "styled-components";

const theme = {
  fonts: {
    body: "Shabnam, Roboto ,Tahoma, sans-serif",
    heading: "inherit",
  },
};

export const GlobalStyle = createGlobalStyle`

  
  * {
    margin: 0;
    padding: 0;
    font-family: ${theme.fonts.body};

  }
  
  *,
  *::after,
  *::before {
    box-sizing: inherit;
  }
  a{
    text-decoration: none;
  }
  
  html {
    box-sizing: border-box;

  }
  body {
    background-color: #fff;
    font-family: ${theme.fonts.body};
    margin: 0;
    overflow-x: hidden;
    #root {
    display: flex;
    flex-direction: column;
    min-height: 100vh;
    > main {
    flex: 1;
    margin-top: 100px;
    margin-bottom: 20px;
   
    }
}
  }
`;

export const Container = styled.div`
  padding-left: 3rem;
  padding-right: 3rem;

  @media (max-width: 560px) {
    padding-left: 1rem;
    padding-right: 1rem;
  }
`;

export const Col =styled.div`
  display: flex;
  flex-direction: column;
  justify-content: ${({justify})=> justify};
  align-items: ${({alignItem})=> alignItem};
`
export const Row =styled.div`
  display: flex;
  justify-content: ${({justify})=> justify};
  align-items: ${({alignItem})=> alignItem};
`