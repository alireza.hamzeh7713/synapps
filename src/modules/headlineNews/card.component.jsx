import React from "react";
import {
  CardWrapper,
  CardCol,
  ImageWrapper,
  SubTitle,
  Description,
} from "./headlineNews.styles";


const CardNews = ({ items: { image, title, description } }) => {
  return (
    <>
      <CardWrapper>
        <CardCol>
            <ImageWrapper imgUrl={image} />
          <SubTitle>{title}</SubTitle>
          <Description>{description}</Description>
        </CardCol>
      </CardWrapper>
    </>
  );
};

export default CardNews;
