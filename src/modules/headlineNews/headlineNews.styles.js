import styled from "styled-components";
import { Link } from "react-router-dom";

const CardRow = styled.div`
  display: grid;
  grid-gap: 20px;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  @media (max-width:1100px){
    grid-template-columns: 1fr 1fr 1fr ;
  }
  @media (max-width:800px){
    grid-template-columns: 1fr 1fr ;

  }
  @media (max-width:550px){
    grid-template-columns: 1fr ;

  }
`;
const Title = styled.h2`
    font-size: ${({size})=> size ? size+"px" : "25px"};
    color: black;
    font-weight: 800;
    margin-bottom: 15px;
    
`
const CardWrapper = styled.div`
  margin: 10px 0;
  
`;
const CardCol = styled.div`
  display: flex;
  flex-direction: column;
`;
const ImageWrapper = styled.div`
  width: 100%;
  height: 160px;
  background-image: url(${({ imgUrl }) => imgUrl});
  margin-bottom: 7px;
  background-size: cover;
  background-position: center;
  border-radius: 5px;
`;
const SubTitle = styled(Link)`
  font-size: 14px;
  font-weight: 400;
  line-height: 1.5rem;
  color: #c0c0c0;

  &:hover {
    color: purple;
  }
`;
const Description = styled.span`
  margin-top: 10px;
  font-size: 14px;
  line-height: 1.2rem;
  color: gray;
`;

export { Title,CardRow, CardWrapper, CardCol, ImageWrapper, SubTitle, Description };
