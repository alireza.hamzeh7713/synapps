import React from "react";
import { Container } from "../../globalStyles";
import CardNews from "./card.component";
import { CardRow, Title } from "./headlineNews.styles";
import img1 from "../../assets/images/02.jpg";
import img2 from "../../assets/images/03.jpg";
import img3 from "../../assets/images/04.jpg";
import img4 from "../../assets/images/05.png";
import img5 from "../../assets/images/06.jpg";
import img6 from "../../assets/images/07.jpg";

const data = [
  {
    id: 1,
    title: "Could Covid vaccine be taken as a pill?",
    image: img1,
    description:
      "Researchers are looking at easier ways for people to get the vaccine, including via inhalers and tablets.",
  },
  {
    id: 2,
    title: "YouTube suspends Sky News Australia over Covid",
    image: img2,
    description:
      "The digital giant bans the channel from uploading new content for a week over misinformation breaches.",
  },
  {
    id: 3,
    title:
      "Covid: Pulse oxygen monitors work less well on darker skin, experts say",
    image: img3,
    description:
      "Researchers are looking at easier ways for people to get the vaccine, including via inhalers and tablets.",
  },
  {
    id: 4,
    title: "Four misleading Covid claims fact-checked",
    image: img4,
    description:
      "Debunking claims shared widely on social media about cremations in the UK and US vaccine statistics.",
  },

  {
    id: 5,
    title: "Fauci says US going in wrong direction on Covid",
    image: img5,
    description:
      "President Joe Biden's top medical adviser says cases are rising in areas with low vaccination rates.",
  },
  {
    id: 6,
    title: "Sydney lockdown protesters condemned as selfish",
    image: img6,
    description:
      "It comes after thousands marched through Australian cities to demand an end to lockdown measures.",
  },
];

const HeadlineNews = () => {
  return (
    <>
      <Container>
        <Title>Headline News</Title>
        <CardRow>
          {data.map((item) => (
            <CardNews key={item.id} items={item} />
          ))}
        </CardRow>
      </Container>
    </>
  );
};

export default HeadlineNews;
