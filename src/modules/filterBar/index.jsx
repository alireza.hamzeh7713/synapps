import React, { useEffect, useState } from "react";
import { FilterBySourceService } from "../latestNews/api/service";
import CustomSelect from "../shared/customSelectBox";
import { FilterBarWrapper, FilterBarRow } from "./filterBar.styles";

const languageData = [
  { id: "ar", title: "Arabic" },
  { id: "de", title: "German" },
  { id: "en", title: "English" },
  { id: "es", title: "Spanish" },
  { id: "fr", title: "French" },
  { id: "he", title: "Hebrew" },
  { id: "it", title: "Italian" },
  { id: "nl", title: "Dutch" },
  { id: "no", title: "Norway" },
  { id: "pt", title: "Portuguese" },
  { id: "ru", title: "Russian" },
  { id: "se", title: "Sami" },
  { id: "zh", title: "Chinese" },
];
const soting = [
    { id: "relevancy", title: "Relevancy" },
    { id: "popularity", title: "Popularity" },
    { id: "publishedAt", title: "PublishedAt" },
   
  ];
const FilterBar = ({ onChangeLanguages,onChangeSorting,onChangeSources }) => {
    const [Sources,setSources]=useState([])
    useEffect(()=>{
        FilterBySourceService().then((res)=> setSources(res.sources))
    },[])
  return (
    <>
      <FilterBarWrapper>
        <FilterBarRow>
          <div>
            <CustomSelect
              options={languageData}
              handleChange={onChangeLanguages}
              notFoundMessage='languages'
              placeholder='languages...'
            />
          </div>
          <div>
            <CustomSelect
              options={soting}
              handleChange={onChangeSorting}
              notFoundMessage='sorting'
              placeholder='sort by...'
            />
          </div>
          <div>
            <CustomSelect
              options={Sources}
              handleChange={onChangeSources}
              notFoundMessage='sources'
              placeholder='release sources...'
            />
          </div>
        </FilterBarRow>
      </FilterBarWrapper>
    </>
  );
};

export default FilterBar;
