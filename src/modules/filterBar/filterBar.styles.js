import styled from "styled-components";


const FilterBarWrapper =styled.div`
    margin: 12px 0;
`
const FilterBarRow =styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    grid-gap: 15px;

`

export {FilterBarWrapper,FilterBarRow}