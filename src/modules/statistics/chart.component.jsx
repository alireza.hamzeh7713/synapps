import React, { useEffect, useState } from "react";
import ReactApexChart from "react-apexcharts";


const Chart = ({ items }) => {
  const [json, setJson] = useState([{}]);
  const [deaths, setDeaths] = useState([{ x: "", y: "" }]);
  const [active, setActive] = useState([{ x: "", y: "" }]);
  const [recovered, setRecovered] = useState([{ x: "", y: "" }]);
  const [confirmed, setConfirmed] = useState([{ x: "", y: "" }]);

  useEffect(() => {
    setDeaths(
      items?.data?.map((i) => ({
        x: new Date(i.date).getTime(),
        y: +i.deaths,
      })),
    );
    setActive(
      items?.data?.map((i) => ({
        x: new Date(i.date).getTime(),
        y: +i.active,
      })),
    );
    setRecovered(
      items?.data?.map((i) => ({
        x: new Date(i.date).getTime(),
        y: +i.recovered,
      })),
    );
    setConfirmed(
      items?.data?.map((i) => ({
        x: new Date(i.date).getTime(),
        y: +i.confirmed,
      })),
    );

  }, [items]);

  useEffect(() => {
    setJson([
      {
        name: "deaths",
        data: deaths,
      },
      { name: "recovered", data: recovered },
      { name: "active", data: active },
      { name: "confirmed", data: confirmed },
    ]);
  }, [deaths]);
  console.log(json);
  const [state, setState] = useState({
  
    options: {
      chart: {
        height: 350,
        type: "line",
        zoom: {
          enabled: false,
        },
      },
      dataLabels: {
        enabled: false,
      },
      stroke: {
        curve: "straight",
      },
      colors: ["#f56565", "#48bb78", "#2d3748", "#a0aec0"],

      grid: {
        row: {
          colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
          opacity: 0.5,
        },
      },
      xaxis: {
        type: "datetime",
      },
    },
  });

  return (
    <>
      <div style={{ marginTop: "40px" }}>
        <ReactApexChart
          options={state.options}
          series={json}
          type='line'
          height={350}
        />
      </div>
    </>
  );
};

export default Chart;
