import {FaHospitalAlt} from 'react-icons/fa'
import {GiHeartPlus,GiDeathSkull} from 'react-icons/gi';

const DynamticFunction = () => {
  const SelectColor = (item) => {
    if (item === "Active") {
      return "#2d3748";
    }
    if (item === "Confirmed") {
      return "#a0aec0";
    }
    if (item === "Recovered") {
      return "#48bb78";
    }
    if (item === "Deaths") {
      return "#f56565";
    }
}
    const SelectIcon = (item) => {
        if (item === "Active") {
          return <FaHospitalAlt color="#c0c0c0"/>
        }
        if (item === "Confirmed") {
          return <FaHospitalAlt />
        }
        if (item === "Recovered") {
          return <GiHeartPlus />
        }
        if (item === "Deaths") {
          return <GiDeathSkull />
        }
    }
    return {SelectColor,SelectIcon}
}
export default DynamticFunction