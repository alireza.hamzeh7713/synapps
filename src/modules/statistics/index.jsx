import React, { useEffect, useState } from "react";
import { Container } from "../../globalStyles";
import StatisticsCard from "./statisticsCard.component";
import { StatisticsWrapper, GridRow } from "./statistics.styles";
import Chart from "./chart.component";
import GetLatestStatisticsQuery from "./api/statisticsQuery";

// const data = [
//   {
//     active: 200000,
//     confirmed: 56622,
//     recovered: 4156225,
//     death: 859522,
//   },
// ];
const Statistics = () => {
  const [perHour,setPerHour]=useState(3600000)
  const { data } = GetLatestStatisticsQuery({
    onSuccess: (data) => console.log(data?.data[0]),
    refetchInterval:perHour
  });

  return (
    <>
      <Container>
        <StatisticsWrapper>
          <GridRow>
            <StatisticsCard items={!!data?.data.length && data?.data[0]} />
          </GridRow>
          <Chart items={data}/>
        </StatisticsWrapper>
      </Container>
    </>
  );
};

export default Statistics;
