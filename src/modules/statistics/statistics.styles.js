import styled from "styled-components";


const StatisticsCardWrapper =styled.div`
    padding: 10px;
    background-color: ${({bgColor})=> bgColor};
    border-radius: 5px;

`
const IconWrapper =styled.div`
    margin: 5px ;

    & > svg{
        font-size: 40px;
    }
`
const KeyItem = styled.span`
    font-size: 15px;
    font-weight: 700;
    color: ${({color})=> color === "Active" ? "#c0c0c0" : "#1a202c"};
    margin: 5px 0;
`
const DataItem = styled.span`
    font-size: 32px;
    font-weight: 700;
    color: #fff;
    margin: 5px 0;

`
const GridRow = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    grid-gap: 20px;

    @media (max-width:910px){
        grid-template-columns: 1fr 1fr 1fr;
  
    }
    @media (max-width:740px){
        grid-template-columns: 1fr 1fr;
  
    }
    @media (max-width:550px){
        grid-template-columns: 1fr;
  
    }
`
const StatisticsWrapper =styled.div`
    margin: 40px 0;
`
export {StatisticsCardWrapper,IconWrapper,KeyItem,DataItem,GridRow,StatisticsWrapper}