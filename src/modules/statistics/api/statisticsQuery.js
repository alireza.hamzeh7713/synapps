import { useQuery } from "react-query";
import GetLatestStatistics from "./service";

const GetLatestStatisticsQuery = ({ onSuccess,refetchInterval }) => {
  return useQuery("getLatestStatistics", GetLatestStatistics, {
    onSuccess,refetchInterval
  });
};

export default GetLatestStatisticsQuery;
