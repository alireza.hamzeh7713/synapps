import axios from "axios"


const GetLatestStatistics =async()=>{
    const {data}= await axios.get("https://corona-api.com/timeline")
    return data
}

export default GetLatestStatistics