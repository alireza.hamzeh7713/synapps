import React, { useState } from "react";
import { Col } from "../../globalStyles";
import DynamticFunction from "./dynamicFunction.component";
import {
  StatisticsCardWrapper,
  KeyItem,
  DataItem,
  IconWrapper,
} from "./statistics.styles";

const StatisticsCard = ({ items }) => {
  const [data, setData] = useState({
    Active: items.active,
    Confirmed: items.confirmed,
    Recovered: items.recovered,
    Deaths: items.deaths,
  });
  const { SelectColor, SelectIcon } = DynamticFunction();

  return (
    <>
      {Object.keys(data).map((item) => (
        <StatisticsCardWrapper bgColor={SelectColor(item)}>
          <Col alignItem='center'>
            <IconWrapper>{SelectIcon(item)}</IconWrapper>
            <KeyItem color={item}>{item}</KeyItem>
            <DataItem>
              {items[item.toLowerCase()]?.toLocaleString(undefined, {
                maximumFractionDigit: 2,
              })}
            </DataItem>
          </Col>
        </StatisticsCardWrapper>
      ))}
    </>
  );
};

export default StatisticsCard;
