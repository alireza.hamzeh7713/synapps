import styled from "styled-components";


const NewsCardWrapper =styled.div`
    margin: 20px 0;
    max-width: 500px;
    width: 100%;
`

const Date = styled.div`
    display: flex;
    align-items: center;
    margin: 20px 0;

    & svg{
        margin-right: 5px;
    }
    & span{
        font-size: 12px;
        color: gray;
    }
`

const ShareWrapper = styled.div`
    display: flex;
    align-items: center;
    margin: 10px 0;
    max-width: 250px;

`
const ShareIcon = styled.div`
    background-color: #B80000;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 10px;
    width: 22%;
    margin-right: 10px;

    & svg{
        color: #f0f0f0;
    }
`
const ShareTitle = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 10px;
    width: 70%;
    border: 1px solid #B80000;
    font-size: 13px;
    color: gray;

`

const Line = styled.div`
    width: 100%;
    height: ${({height})=> height ? height +"px" : "2px"};
    background-color: ${({bgColor})=> bgColor ? bgColor  : "#c0c0c0"};
    margin:10px 0 ;
`

const Image =styled.img`
    /* background-image: url(${({imgUrl})=> imgUrl}); */
    /* background-size: cover; */
    /* object-fit: contain;
    background-position: center; */
    width: 100%;
    height: 300px;
    margin: 20px 0 ;

`

const TextWrapper = styled.div`
    display: flex;
    align-items: center;
    color: gray;

    & >span{
        font-size: 14px;
    }
    & span:first-child{
        margin-right: 5px;
    }
`

export {NewsCardWrapper,Date,ShareWrapper,ShareIcon,ShareTitle,Line,Image,TextWrapper}