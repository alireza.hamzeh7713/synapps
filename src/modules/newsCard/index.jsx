import React from "react";
import { Description, Title } from "../headlineNews/headlineNews.styles";
import { BiShareAlt } from "react-icons/bi";
import { AiOutlineClockCircle } from "react-icons/ai";
import {
  NewsCardWrapper,
  Date,
  ShareWrapper,
  ShareIcon,
  ShareTitle,
  Line,
  Image,
  TextWrapper,
} from "./news.styles";
const NewsCard = ({
  items: { author, title, description, urlToImage, publishedAt, source },
}) => {
    
  return (
    <>
      <NewsCardWrapper>
        <Title size='30'>{title}</Title>
        <Date>
          <AiOutlineClockCircle color='gray' />
          <span>{publishedAt}</span>
        </Date>
        <ShareWrapper>
          <ShareIcon>
            <BiShareAlt />
          </ShareIcon>
          <ShareTitle>Cornavirus Pandemic</ShareTitle>
        </ShareWrapper>
        <Line />

        <Image src={urlToImage} alt="can't loaded image"/>
        <Description>{description}</Description>
        <Line bgColor='#B80000' height='4' />
        <TextWrapper>
          <span>author:</span>
          <span>{author}</span>
        </TextWrapper>
        <TextWrapper>
          <span>source:</span>
          <span>{source?.name}</span>
        </TextWrapper>
      </NewsCardWrapper>
    </>
  );
};

export default NewsCard;
