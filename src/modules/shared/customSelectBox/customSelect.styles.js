const selectFilledStyles = {
    container: provided => ({
      ...provided,
      width: '100%',
      // height: '100%',
    }),
    placeholder: provided => ({
      ...provided,
      color: "#c0c0c0",
      // fontFamily: 'inherit',
      fontSize:'0.8rem'
    }),
    option: provided => ({
      ...provided,
      fontSize:"13px",
      direction:'ltr'

  
    }),
    control: provided => ({
      ...provided,
      width: '100%',
      height: '100%',
      // minHeight: '39px',
      border: 'none',
      boxShadow: 'none',
      padding: '0 15px',
      cursor: 'pointer',
      borderRadius: '5px',
      backgroundColor: "#ffff",
      border: "1px solid #e0e0e0",
      direction:'ltr'

    }),
    indicatorSeparator: provided => ({
      ...provided,
      backgroundColor: 'transparent',
    }),
    singleValue: provided => ({
      ...provided,
      color: "#c0c0c0",
      fontWeight: 400,
      fontSize:'0.9rem',
      direction:'ltr'

    }),
    input: provided => ({
      ...provided,
      color: "white",
      direction:'ltr'
    }),
    clearIndicator: provided => ({
      ...provided,
      position: 'absolute',
      left: '10px',
    }),
  };
  
  export {  selectFilledStyles };
  