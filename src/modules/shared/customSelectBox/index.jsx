import React from 'react'
import Select from 'react-select'
import { selectFilledStyles } from './customSelect.styles'

const CustomSelect = ({options,handleChange,notFoundMessage,placeholder}) => {
    return (
        <>
             <Select
                  styles={selectFilledStyles}
                  options={options}
                  getOptionValue={(option) => option.id}
                  getOptionLabel={(option) => option.title || option.name}
                  onChange={handleChange}
                  noOptionsMessage={() => `${notFoundMessage}  not found`}
                  placeholder={placeholder}
                  isRtl
                />
            
        </>
    )
}

export default CustomSelect
