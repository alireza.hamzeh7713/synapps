import { Link } from "react-router-dom";
import styled from "styled-components";

export const HeaderWrapper = styled.div`
  width: 100%;
  position: fixed;
  z-index: 100;
  top: 0;
  left: 0;
  background-color: #bb1919;
  border-bottom: 1px solid #979797;
`;
export const HeaderRow = styled.div`
  height: 70px;
  display: flex;
  margin: 0 20px;
  align-items: center;
`;
export const LogoWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;
export const HeaderLogo = styled(Link)`
  font-size: 1.5rem;
  color: #fff;
  font-weight: 500;
  margin-left: 0rem;
  @media (max-width: 800px) {
    margin-left: 1rem;
  }
`;

export const Nav = styled.nav`
  flex: 1;
  @media (max-width: 800px) {
    display: none;
  }
`;

export const NavWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  direction: rtl;

  & > span {
    margin-right: 2rem;
    font-weight: 400;
    font-size: 1rem;
    color: #ffff;
  }
`;
