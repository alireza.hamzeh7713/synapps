import React from "react";
import {
  HeaderWrapper,
  HeaderRow,
  LogoWrapper,
  HeaderLogo,
  NavWrapper,
  Nav,
} from "./header.styles";
import HamLogo from "../sideMenu/hamburgerIcon";

const data = [
  { id: 1, title: "Home" },
  { id: 2, title: "About" },
  { id: 3, title: "Tell us" },
  { id: 4, title: "Social media" },
  { id: 5, title: "Questions" },
];

const Header = ({ LogoClick }) => {
  return (
    <HeaderWrapper>
      <HeaderRow>
        <LogoWrapper>
          <HamLogo click={LogoClick} />
          <HeaderLogo to="/">NEWS</HeaderLogo>
        </LogoWrapper>

        <Nav>
          <NavWrapper>
            {data.map((item) => (
              <span key={item.id}>{item.title}</span>
            ))}
          </NavWrapper>
        </Nav>
      </HeaderRow>
    </HeaderWrapper>
  );
};

export default Header;
