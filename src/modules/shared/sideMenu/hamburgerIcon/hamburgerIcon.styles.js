import styled from "styled-components";

export const HamLogoWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  height: 25px;
  color: white;
  cursor: pointer;
  background-color: transparent;
  border: none;
  &:focus {
    outline: none;
  }

  @media (min-width: 800px) {
    display: none;
  }
`;
export const HamLogoChild = styled.div`
  height: 2px;
  width: 25px;
  background-color: #fff;
`;
