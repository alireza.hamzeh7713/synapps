import React, { useState } from "react";
import { HamLogoWrapper, HamLogoChild } from "./hamburgerIcon.styles";

const HamLogo = ({ click }) => {
  const [counter, setCounter] = useState(new Array(3).fill(""));
  return (
    <>
      <HamLogoWrapper onClick={click}>
        {counter.map((_, index) => (
          <HamLogoChild key={index} />
        ))}
      </HamLogoWrapper>
    </>
  );
};

export default HamLogo;
