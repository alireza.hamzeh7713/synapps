import styled from "styled-components";


export const SideNavWrapper = styled.div`
  max-width: 300px;
  width: 100%;
  position: fixed;
  background: #fafafa;
  left: 0;
  top: 0;
  height: 100%;
  border-right: 1px solid #979797;
  transform: ${({ show }) => (show ? "translateX(0)" : "translateX(-100%)")};
  transition: transform 0.3s;
  z-index: 101;
  overflow-y: hidden;
`;
export const SideNavCol = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 50px auto;
  max-height: 400px;
  height: 100%;
`;
export const SideLogoWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;
export const SideNavLogo = styled.div`
  width: 100px;
  height: 100px;
  border-radius: 50%;
  background-color: #bb1919;
  border: 1px solid gray;
  display: flex;
  justify-content: center;
  align-items: center;

  & span{
      color:#fff;
  }
`;
export const SideNavItemWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 5rem;
  padding-left: 1rem;
  padding-right: 1rem;
  width: 100%;

  & > span {
    background-color: #fff;
    width: 100%;
    text-align: center;
    color: black;
    border-radius: 7px;
    margin: 1rem 0;
    padding: 1rem;
    /* box-shadow: 0 6px 5px 0 rgb(0 0 0 / 6%); */
    box-shadow: 0 5px 10px rgba(154,160,185,.05), 0 15px 40px rgba(166,173,201,.2);}
    `
