import React from "react";
import {
  SideNavWrapper,
  SideNavCol,
  SideNavLogo,
  SideLogoWrapper,
  SideNavItemWrapper,
} from "./sideMenu.styles";

const data = [
    { id: 2, title: "About" },
    { id: 3, title: "Tell us" },
    { id: 4, title: "Social media" },
    { id: 5, title: "Questions" },
  ];

const SideNav = ({ showSide }) => {
  return (
    <>
      <SideNavWrapper show={showSide}>
        <SideNavCol>
          <SideLogoWrapper>
            <SideNavLogo ><span>NEWS</span></SideNavLogo>
          </SideLogoWrapper>

          <SideNavItemWrapper>
              {data.map((item)=>(
                  <span key={item.id}>{item.title}</span>
              ))}
           
          </SideNavItemWrapper>
        </SideNavCol>
      </SideNavWrapper>
    </>
  );
};

export default SideNav;
