import React from "react";
import { BackDropStyle } from "./backDrop.styles";


const BackDrop = ({ click }) => {
  return (
    <>
      <BackDropStyle onClick={click} />
    </>
  );
};

export default BackDrop;
