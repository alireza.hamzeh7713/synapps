import { useInfiniteQuery, useMutation, useQuery } from "react-query";
import { LatestNewsService } from "./service";

// const InfiniteNewsQuery = ({
//   language = "en",
//   sort = "publishedAt",
//   onSuccess,
// }) => {
//   return useInfiniteQuery(
//     "infiniteNews",
//      (key,nextId = 1) => LatestNewsService({pageNumber:nextId ,language,sort }),

//     {
//       onSuccess,
//       // getPreviousPageParam: (firstPage) => firstPage.previousId ?? false,

//       getFetchMore: (lastGroup) => lastGroup?.nextId

//     },
//   );
// };

// export { InfiniteNewsQuery };

const InfiniteScrollQuery = ({
  pageNumber,
  language,
  sortBy,
  sources,
  onSuccess,
  onError,
  enabled,
}) => {
  return useQuery(
    ["infiniteQuery", { language, pageNumber, sortBy, sources }],
     () => LatestNewsService({ pageNumber, language, sortBy, sources }),
    {
      onSuccess,
      onError,
      enabled,
    },
  );
};

export default InfiniteScrollQuery;
