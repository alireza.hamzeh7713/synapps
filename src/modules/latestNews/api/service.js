import { baseUrl } from "../../../constants";
import axios from "axios";
const API_KEY = "9506f1e646cd41ddb0dff1ffe04843ed";

export const LatestNewsService = async ({
  pageNumber,
  language,
  sortBy,
  sources,
}) => {
  const { data } = await axios.get(
    `${baseUrl}/everything?q=covid&sources=${sources}&pageSize=5&page=${pageNumber}&language=${language}&sortBy=${sortBy}&apiKey=${API_KEY}`,
  );
  return data;
};

export const FilterBySourceService = async () => {
  const { data } = await axios.get(
    `https://newsapi.org/v2/top-headlines/sources?apiKey=${API_KEY}`,
  );
  return data;
};
