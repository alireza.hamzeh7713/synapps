import React, { useEffect, useRef, useState } from "react";
import { Col, Container } from "../../globalStyles";
import { Title } from "../headlineNews/headlineNews.styles";
import { NewsTitle, LatestNewsWrapper, NewsLink } from "./latestNews.styles";
import FilterBar from "../filterBar";
import InfiniteScrollQuery from "./api/query";
import { v4 as uuidv4 } from "uuid";
import { useHistory } from "react-router";

const LatestNews = () => {
  const history =useHistory()
  const [isFetching, setIsFetching] = useState(false);
  const [totalResult,setTotalResult]=useState(0)
  const [page, setPage] = useState(1);
  const [data, setData] = useState([]);
  const [language, setLanguage] = useState("en");
  const [sort, setSort] = useState("publishedAt");
  const [sources, setSources] = useState("");

   const {isError ,error} =InfiniteScrollQuery({
    pageNumber: page,
    language: language,
    sortBy: sort,
    sources,
    onSuccess: (res) => {
      console.log(res);
      if(res?.articles?.length){
      setData([...data, ...res.articles.map((article)=> ({...article,uniqueId:uuidv4()}))]);
      setPage(page + 1);
      setIsFetching(false);
      setTotalResult(res?.totalResults)
    }
    },
    onError:(error)=>{
      if(error?.message){
        setIsFetching(false)

      }

    },
    enabled:isFetching
  });



    const loadMoreData =()=>{
      if(totalResult > data.length){
        return
      }else{
        setIsFetching(false)
      }
    }

  
  const isScrolling = () => {
    if (
      window.innerHeight + document.documentElement.scrollTop !==
      document.documentElement.offsetHeight
    ) {
      return;
    } else {
      setIsFetching(true);
      
    }
  };
  useEffect(() => {
    window.addEventListener("scroll", isScrolling);
    return () => window.removeEventListener("scroll", isScrolling);
  }, []);

 


  const handleLanguage = (e) => {
    const { id } = e;
    setLanguage(id);
    setData([])
    setPage(1)

  };


  const handleSorting = (e) => {
    const { id } = e;
    setSort(id);
    setData([])
    setPage(1)


  };
  const handleSources = (e) => {
    const { id } = e;
    setSources(id);
    setData([])
    setPage(1)

  };

  const handleSendNewsPage =(id)=>{
    history.push({pathname: `/news/${id}`,
    state: {data}
    })
  }
  return (
    <>
      <Container>
        <LatestNewsWrapper>
          <Title>Latest News</Title>
          <FilterBar
            onChangeLanguages={handleLanguage}
            onChangeSorting={handleSorting}
            onChangeSources={handleSources}
          />

          <>
            <Col>
              <>
                {data?.map((item, index) => (
                  <NewsTitle>
                    <NewsLink onClick={()=>handleSendNewsPage(item.uniqueId)} key={index}>{item.title}</NewsLink>
                  </NewsTitle>
                ))}
                {isFetching ? "loading..." : isError && <span>{error.message}</span>}

              </>
            </Col>
          </>
          {/* )} */}
          {/* <button
            ref={loadMoreButtonRef}
            onClick={() => fetchMore()}
            disabled={!canFetchMore || isFetchingMore}>
            {isFetchingMore
              ? "Loading more..."
              : canFetchMore
              ? "Load More"
              : "Nothing more to load"}
          </button> */}
        </LatestNewsWrapper>
      </Container>
    </>
  );
};

export default LatestNews;
