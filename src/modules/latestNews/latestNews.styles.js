import styled from "styled-components";
import {Link} from 'react-router-dom'

const LatestNewsWrapper = styled.div`   
    margin-top: 55px;
    max-width: 600px;
    width: 100%;
`
const NewsTitle =styled.div`
    border-radius: 7px;
    padding: 10px;
    box-shadow: 0 6px 5px 0 rgb(0 0 0 / 6%);
      margin-bottom: 18px;
    `
const NewsLink =styled.span`
    font-size: 13px;
    font-weight: 400;
    color: gray;
    margin: 10px 0;
    word-wrap:break-word;
    line-height: 1.5rem;
    background-color: transparent;
    cursor: pointer;
    
    &:hover{
        color: purple;
    }
`



export {NewsTitle,NewsLink,LatestNewsWrapper}