export const baseUrl =
  process.env.NODE_ENV === 'production'
    ? 'https://newsapi.org/v2'
    : 'https://newsapi.org/v2';

