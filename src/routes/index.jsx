import React from 'react'
import { Route, Switch } from 'react-router'
import HomePage from '../pages/homePage'
import NewsPage from '../pages/newsPage'

const Routes = () => {
    return (
        <>
           <Switch>
             <Route exact path="/" component={HomePage}/>
             <Route path="/news/:id" component={NewsPage}/>

            </Switch> 
        </>
    )
}

export default Routes
