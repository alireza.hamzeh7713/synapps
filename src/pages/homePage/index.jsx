import React from "react";
import HeadlineNews from "../../modules/headlineNews";
import LatestNews from "../../modules/latestNews";
import Statistics from "../../modules/statistics";

const HomePage = () => {
  return (
    <>
        <HeadlineNews />
        <Statistics />
        <LatestNews />
    </>
  );
};

export default HomePage;
