import React, { useEffect, useState } from "react";
import { useQueryClient } from "react-query";
import { useLocation, useParams } from "react-router";
import { Container } from "../../globalStyles";
import NewsCard from "../../modules/newsCard";

const NewsPage = () => {
  const { state } = useLocation();
  const { id } = useParams();
  const [data, setData] = useState({});
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setData(...state.data.filter((item) => item.uniqueId === id));
    setIsLoading(!data ? true : false);
  }, []);
  return (
    <div>
      <Container>
        {isLoading ? <span>loading...</span> : <NewsCard items={data} />}
      </Container>
    </div>
  );
};

export default NewsPage;
